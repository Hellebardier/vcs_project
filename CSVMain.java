package ch.adrian_brunner.csv_code;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/*
 * CSVMain
 * @author Adrian Brunner
 * @version 27.10.2020
 * 
 * Dieses Programm liest CSV Dateien und lässt diese nach den Vorgaben mit calibration points ausrechnen.
 */

public class CSVMain {

	public static void main(String[] args) {

		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV files", "csv");
		chooser.setFileFilter(filter);
		int returnVal = chooser.showOpenDialog(null);
		String fileName = null;
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = chooser.getSelectedFile();
			fileName = file.getAbsolutePath();
		}

		Scanner scan = new Scanner(System.in);
		System.out.println("Bitte geben Sie den gewünschten Filenamen, für die neue CSV Datein an: ");
		String neuFile = scan.next();

		System.out.println("Bitte wählen Sie die gewünschte Anzahl 'calibration points': ");
		String point = scan.next();
		int calpoint = Integer.parseInt(point);

		String line = "";

		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));

			FileWriter fw = new FileWriter(neuFile);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);

			double[] wert = new double[0];

			while ((line = br.readLine()) != null) {
				String[] values = line.split(";");
				double[] neuWerte = new double[values.length];
				for (int i = 0; i < neuWerte.length; i++) {
					neuWerte[i] = Double.parseDouble(values[i]);
				}
				
				boolean specialRow = false;

				wert = new double[neuWerte.length + ((neuWerte.length - 3) * calpoint)];
				for (int i = 0; i < neuWerte.length; i++) {
					if (i > 1 && i < neuWerte.length - 1) {
						int neuIndex = i + ((i - 2) * calpoint);
						wert[neuIndex] = neuWerte[i];
						double diff = neuWerte[i + 1] - neuWerte[i];
						for (int j = 1; j <= calpoint; j++) {
						if (specialRow) {
								wert[neuIndex+j] = neuWerte[i];
								continue;
							}
							wert[neuIndex + j] = Math.round(neuWerte[i] + ((diff / (calpoint + 1)) * j));
						}
					} else if (i < 2){
						wert[i] = neuWerte[i];
						if (neuWerte[i] == 65003 ||neuWerte[i] == 65004) {
							specialRow = true;
						}
					} else {
						wert[neuWerte.length + ((neuWerte.length - 3) * calpoint) -1] = neuWerte[i];
					}
				}

				for (int i = 0; i < wert.length; i++) {
					pw.print(wert[i] + ";");
				}
				pw.println();

			}

			pw.flush();
			pw.close();
			JOptionPane.showMessageDialog(null, "Record saved");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
